'''
Created on 13.12.2012

@author: horizonOffset
'''
from UMResourceLoader import UMResourceLoader
import os.path
import pygame
from UMException import UMException

class UMImageLoader(UMResourceLoader):
    
    def __init__(self, pool):
        self.pool = pool
        self.location = os.path.join(pool.location, 'images')
        
    def load(self, name):
        fullpath = os.path.join(self.location, name + '.png')
        
        try:
            image = pygame.image.load(fullpath)
        except pygame.error, exception:
            raise UMException('Failed to load image "' + fullpath + '":\n' + str(exception))
        
        image = image.convert_alpha()
        
        return image
