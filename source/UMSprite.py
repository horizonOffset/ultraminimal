'''
Created on 14.12.2012

@author: horizonOffset
'''
from UMSceneNode import UMSceneNode
import pygame

class UMSprite(UMSceneNode):
    
    def __init__(self, image):
        super(UMSprite, self).__init__(UMSceneNode.Type.DRAWABLE)
        self.image = image
        self.rect = image.get_rect()
        
    def changeImage(self, image):
        self.image = image
        self.rect = image.get_rect()
        
    def draw(self, surface):
        surface.blit(self.image, self.worldTransform.xy())
        
    def createEmpty(self, size):
        return UMSprite(pygame.Surface(size))
