'''
Created on 14.12.2012

@author: horizonOffset
'''
from UMSceneNode import UMSceneNode

class UMLayerGroup(UMSceneNode):

    def __init__(self):
        super(UMLayerGroup, self).__init__(UMSceneNode.Type.GROUP)
        
        self.childNodes = []
        
    def addChild(self, child):
        if not child in self.childNodes:
            self.childNodes.append(child)
            child.parent = self
            
        return child
        
    def removeChild(self, child):
        if child in self.childNodes:
            self.childNodes.remove(child)
            child.parent = None
            
        return child
        
    def replaceChild(self, child, replacement):
        self.childNodes = [replacement if x == child else x for x in self.childNodes]
        
    def setup(self, runtime):
        for child in self.childNodes:
            child.setup(runtime)
            
        self.onSetup(runtime)
        self.updateTransform()
        
    def update(self, runtime, deltaTime):
        for child in self.childNodes:
            child.update(runtime, deltaTime)
            
        self.onUpdate(runtime, deltaTime)
        self.updateTransform()
            
    def filter(self, condition):
        result = []
        
        if self.active:
            for child in self.childNodes:
                if child.type == UMSceneNode.Type.GROUP:
                    result.extend(child.filter(condition))
                elif condition(child):
                    result.append(child)
                    
        return result
