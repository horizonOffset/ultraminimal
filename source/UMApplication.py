'''
Created on 12.12.2012

@author: horizonOffset
'''

class UMApplication(object):
    
    def setup(self, runtime): pass
    def init(self, runtime): pass
    def drop(self, runtime): pass
    def draw(self, runtime): pass
    def step(self, runtime, deltaTime): pass
