'''
Created on 12.12.2012

@author: horizonOffset
'''
import pygame
from UMWindow import UMWindow
from UMResourcePool import UMResourcePool
from UMLogger import UMLogger
from UMException import UMException
from UMRenderer import UMRenderer
from UMInput import UMInput

class UMRuntime(object):

    FRAMERATE = 60

    def __init__(self, loggerOutput, resourceLocation):
        self.window     = UMWindow(self)
        self.pool       = UMResourcePool(self, resourceLocation)
        self.renderer   = UMRenderer(self.window)
        self.logger     = UMLogger(loggerOutput)
        self.input      = UMInput()
        
    def run(self, application, caption):
        try:
            self.logger.send('\nPreparing')
            application.setup(self)
            
            self.logger.send('\nInitializing')
            pygame.init()
            self.window.create(caption)
            self.pool.create()
            
            application.init(self)
            self.mainloop(application)
        except UMException, exception:
            self.logger.sendError('Exception caught:\n' + str(exception))
        
        self.logger.send('\nTerminating')
        application.drop(self)
        pygame.quit()
        
    def mainloop(self, application):
        self.logger.send('\nRunning')
        
        running = True
        clock = pygame.time.Clock()
        while running:
            application.draw(self)
            self.window.update()
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                else: self.input.processEvent(event)
            
            running = running and application.step(self, clock.tick(self.FRAMERATE))

