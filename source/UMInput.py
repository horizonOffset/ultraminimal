'''
Created on 14.12.2012

@author: horizonOffset
'''
#import pygame
from UMEvent import UMEvent
import pygame

class UMInput(object):
    
    def __init__(self):
        self.pressed = []
        self.keydown = UMEvent()
        self.keyup = UMEvent()
        self.mouseDown = UMEvent()
        self.mouseUp = UMEvent()

    def isKeyDown(self, key): return key in self.pressed
    
    def processEvent(self, event):
        if event.type == pygame.KEYDOWN:
            self.keydown.fire(event.key)
            self.pressed.append(event.key)
        elif event.type == pygame.KEYUP:
            self.keyup.fire(event.key)
            self.pressed.remove(event.key)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self.mouseDown.fire(event.button)
        elif event.type == pygame.MOUSEBUTTONUP:
            self.mouseUp.fire(event.button)
            
    def mousePos(self): return pygame.mouse.get_pos()
