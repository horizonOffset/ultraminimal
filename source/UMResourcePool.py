'''
Created on 12.12.2012

@author: horizonOffset
'''

import os.path
from UMException import UMException
from UMImageLoader import UMImageLoader
from UMSoundLoader import UMSoundLoader

class UMResourcePool(object):
    
    def __init__(self, runtime, location):
        self.runtime = runtime
        self.location = location
        self.pool = {}
        self.loaders = {}
        
        self.addLoader('image', UMImageLoader(self))
        self.addLoader('sound', UMSoundLoader(self))
        
    def create(self):
        self.runtime.logger.send(' - Creating [RESOURCE POOL]')
        self.runtime.logger.send('    {%} Reading resource list')
        
        with open(os.path.join(self.location, 'list'), 'r') as resList:
            for line in resList:
                args = line.split()
                loader = args[0]
                name = args[1]
                
                self.load(name, loader)
        
    def addResource(self, name, resource):
        self.pool[name] = resource
        
    def addLoader(self, name, loader):
        self.loaders[name] = loader
    
    def load(self, name, loader):
        if self.pool.has_key(name):
            return self.pool.get(name)
        else:
            self.runtime.logger.send('    {+} Loading resource "' + name + '"')
            
            if not self.loaders.has_key(loader):
                raise UMException('Unknown resource loader type: "' + loader + '"')
            
            resource = self.loaders.get(loader).load(name)
            self.addResource(name, resource)
            
            return resource
        
    def loadImage(self, name): return self.load(name, 'image')
    
    def loadSound(self, name): return self.load(name, 'sound')
