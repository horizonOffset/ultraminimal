'''
Created on 14.12.2012

@author: horizonOffset
'''

class UMRenderer(object):
    
    def __init__(self, window):
        self.window = window
        
    def draw(self, items, target):
        surface = self.window.surface
        if target is not None:
            surface = target
        
        surface.fill((0, 0, 0))
        for item in items:
            if surface.get_rect().colliderect(item.worldRect):
                item.draw(surface)
