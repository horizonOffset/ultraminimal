'''
Created on 12.12.2012

@author: horizonOffset
'''
import pygame

class UMWindowMode():
    
    def __init__(self, size, fullscreen):
        self.size = size
        self.fullscreen = fullscreen

class UMWindow(object):

    def __init__(self, runtime):
        self.runtime = runtime
        self.mode = UMWindowMode((800, 600), False)
        self.surface = None
        
    def create(self, caption):
        self.runtime.logger.send(' - Creating [WINDOW]')
        self.runtime.logger.send('    Setup: [size = ' + str(self.mode.size) + ', ' +
                                 'fullscreen = ' + str(self.mode.fullscreen) + ']')
        pygame.display.set_mode(self.mode.size, self.mode.fullscreen)
        pygame.display.set_caption(caption)
        
        self.surface = pygame.display.get_surface()
        
    def width(self): return self.surface.get_width()
    def height(self): return self.surface.get_height()
        
    def update(self):
        pygame.display.flip()
