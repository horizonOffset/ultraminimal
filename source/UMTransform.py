'''
Created on 14.12.2012

@author: horizonOffset
'''

class UMTransform(object):

    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def __add__(self, other):
        return UMTransform(self.x + other.x, self.y + other.y)
    
    def xy(self): return (self.x, self.y)
