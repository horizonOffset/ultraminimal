'''
Created on 14.12.2012

@author: horizonOffset
'''
from UMTransform import UMTransform
import pygame

class UMSceneNode(object):

    class Type:
        GROUP, DRAWABLE, SOUND_SOURCE = range(3) 

    def __init__(self, nodeType):
        self.type = nodeType
        self.transform = UMTransform()
        self.worldTransform = UMTransform()
        self.active = True
        self.parent = None
        self.rect = pygame.Rect(0, 0, 0, 0)
        self.worldRect = pygame.Rect(0, 0, 0, 0)
        
    def setup(self, runtime):
        self.onSetup(runtime)
        self.updateTransform()
    
    def update(self, runtime, deltaTime):
        self.onUpdate(runtime, deltaTime)
        self.updateTransform()
        
    def updateTransform(self):
        if self.parent is not None:
            self.worldTransform = self.parent.worldTransform + self.transform
        else:
            self.worldTransform = self.transform
        
        self.worldRect = pygame.Rect(self.worldTransform.xy(), self.rect.size)
    
    def onSetup(self, runtime): pass
    
    def onUpdate(self, runtime, deltaTime): pass
