'''
Created on 12.12.2012

@author: horizonOffset
'''

import logging

class UMLoggerFormatter(logging.Formatter):
    
    def __init__(self, *args, **kwargs):
        logging.Formatter.__init__(self, *args, **kwargs)
    
    def format(self, record):
        if record.levelno == logging.WARNING:
            return 'WARNING! ' + record.msg
        elif record.levelno == logging.ERROR:
            return 'ERROR! ' + record.msg
        
        return record.msg

class UMLogger(object):

    def __init__(self, outputPath):
        self.logger = logging.getLogger()
        
        with open(outputPath, 'w'): pass
        handler = logging.FileHandler(outputPath)
        handler.setFormatter(UMLoggerFormatter())
        handler.setLevel(logging.INFO)
        
        self.logger.addHandler(handler)
        self.logger.setLevel(logging.INFO)
        
    def send(self, message):
        self.logger.info(message)
    
    def sendWarning(self, message):
        self.logger.warning(message)
    
    def sendError(self, message):
        self.logger.error(message)
