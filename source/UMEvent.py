'''
Created on 14.12.2012

@author: horizonOffset
'''

class UMEvent(object):

    def __init__(self):
        self.subscribers = []
        
    def subscribe(self, subscriber):
        self.subscribers.append(subscriber)
        
    def fire(self, value):
        for subscriber in self.subscribers:
            subscriber(value)
