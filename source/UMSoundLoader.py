'''
Created on 14.12.2012

@author: horizonOffset
'''
from UMResourceLoader import UMResourceLoader
import os.path
import pygame
from UMException import UMException

class UMSoundLoader(UMResourceLoader):
    
    def __init__(self, pool):
        self.pool = pool
        self.location = os.path.join(pool.location, 'sounds')
        
    def load(self, name):
        fullpath = os.path.join(self.location, name + '.wav')
        
        try:
            sound = pygame.mixer.Sound(fullpath)
        except pygame.error, exception:
            raise UMException('Failed to load sound "' + fullpath + '":\n' + str(exception))
            
        return sound
