'''
Created on 14.12.2012

@author: horizonOffset
'''
from UMLayerGroup import UMLayerGroup
from UMSceneNode import UMSceneNode

class UMScene(UMLayerGroup):

    def __init__(self, runtime):
        super(UMScene, self).__init__()
        self.runtime = runtime

    def draw(self, target = None):
        drawableNodes = self.filter(lambda x: x.type == UMSceneNode.Type.DRAWABLE)
        self.runtime.renderer.draw(drawableNodes, target)
