'''
Created on 13.12.2012

@author: horizonOffset
'''

class UMException(Exception):
    
    def __init__(self, *args, **kwargs):
        super(UMException, self).__init__(*args, **kwargs)
